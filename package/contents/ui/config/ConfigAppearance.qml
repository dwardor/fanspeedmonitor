/*
    SPDX-FileCopyrightText: 2023 Oliver Beard <olib141@outlook.com>
    SPDX-License-Identifier: MIT
*/

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2

import org.kde.kcmutils as KCM
import org.kde.kirigami as Kirigami

KCM.SimpleKCM {

    property bool cfg_showUnit
    property bool cfg_enableDangerColor
    property int cfg_warningThreshold
    property int cfg_meltdownThreshold
    property bool cfg_swapLabels
    property bool cfg_isoFont
    property double cfg_primaryFontScale
    property double cfg_secondaryFontScale
    property double cfg_secondaryLabelOpacity
    property bool cfg_secondaryLabelCentered

    // HACK: Present to suppress errors
    property string cfg_sensors
    property string cfg_sensorsDefault
    property bool cfg_showUnitDefault
    property bool cfg_swapLabelsDefault
    property bool cfg_isoFontDefault
    property double cfg_primaryFontScaleDefault
    property double cfg_secondaryFontScaleDefault
    property double cfg_secondaryLabelOpacityDefault
    property bool cfg_secondaryLabelCenteredDefault
    property double cfg_updateInterval
    property double cfg_updateIntervalDefault

    onCfg_showUnitChanged: { showUnitBox.checked = cfg_showUnit; }
    onCfg_swapLabelsChanged: {
        primaryLabelFanspeedButton.checked = !cfg_swapLabels;
        primaryLabelNameButton.checked = cfg_swapLabels;
    }
    onCfg_isoFontChanged: { isoFontBox.checked = cfg_isoFont; }
    onCfg_primaryFontScaleChanged: { primaryFontScaleSpinBox.value = primaryFontScaleSpinBox.toInt(cfg_primaryFontScale); }
    onCfg_secondaryFontScaleChanged: { secondaryFontScaleSpinBox.value = secondaryFontScaleSpinBox.toInt(cfg_secondaryFontScale); }
    onCfg_secondaryLabelOpacityChanged: { secondaryLabelOpacitySpinBox.value = secondaryLabelOpacitySpinBox.toInt(cfg_secondaryLabelOpacity); }
    onCfg_secondaryLabelCenteredChanged: { secondaryLabelCenteredBox.checked = cfg_secondaryLabelCentered; }

    Component.onCompleted: cfg_swapLabelsChanged()

    Kirigami.FormLayout {

        QQC2.CheckBox {
            id: showUnitBox
            Kirigami.FormData.label: "Fan speed:"

            text: "Show unit"
            onCheckedChanged: { cfg_showUnit = checked; }
        }

        Item { Kirigami.FormData.isSection: true }

        QQC2.ButtonGroup { id: primaryLabelGroup }

        QQC2.RadioButton {
            id: primaryLabelFanspeedButton
            Kirigami.FormData.label: "Primary label:"

            text: "Fan speed"
            onCheckedChanged: cfg_swapLabels = !checked
        }

        QQC2.RadioButton {
            id: primaryLabelNameButton

            text: "Name"
            onCheckedChanged: cfg_swapLabels = checked
        }

        RowLayout {
            QQC2.Label {
                text: "Font scale:"
            }
            QQC2.SpinBox {
                id: primaryFontScaleSpinBox

                stepSize: toInt(0.1)
                from: toInt(0.0)
                to: toInt(12.0)

                validator: DoubleValidator {
                    bottom: primaryFontScaleSpinBox.from
                    top: primaryFontScaleSpinBox.to
                    decimals: 1
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: (value, locale) => {
                    return Number(fromInt(value)).toLocaleString(locale, 'f', 1);
                }

                valueFromText: (text, locale) => {
                    return Math.round(toInt(Number.fromLocaleString(locale, text)));
                }

                onValueChanged: { cfg_primaryFontScale = fromInt(value); }

                function toInt(value) {
                    return value * 10;
                }

                function fromInt(value) {
                    return value / 10;
                }
            }
        }

        Item { Kirigami.FormData.isSection: true }

        QQC2.CheckBox {
            id: isoFontBox
            Kirigami.FormData.label: "Secondary label:"

            text: "Same Font as Primary label"
            onCheckedChanged: { cfg_isoFont = checked; }
        }

        QQC2.CheckBox {
            id: secondaryLabelCenteredBox

            text: "Centered"
            onCheckedChanged: { cfg_secondaryLabelCentered = checked; }
        }

        RowLayout {
            QQC2.Label {
                text: "Font scale:"
            }
            QQC2.SpinBox {
                id: secondaryFontScaleSpinBox
                //Kirigami.FormData.label: "Font scale:"

                stepSize: toInt(0.1)
                from: toInt(0.0)
                to: toInt(12.0)

                validator: DoubleValidator {
                    bottom: secondaryFontScaleSpinBox.from
                    top: secondaryFontScaleSpinBox.to
                    decimals: 1
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: (value, locale) => {
                    return Number(fromInt(value)).toLocaleString(locale, 'f', 1);
                }

                valueFromText: (text, locale) => {
                    return Math.round(toInt(Number.fromLocaleString(locale, text)));
                }

                onValueChanged: { cfg_secondaryFontScale = fromInt(value); }

                function toInt(value) {
                    return value * 10;
                }

                function fromInt(value) {
                    return value / 10;
                }
            }
        }

        RowLayout {
            QQC2.Label {
		    text: "Opacity:"
            }
            QQC2.SpinBox {
                id: secondaryLabelOpacitySpinBox

                stepSize: toInt(0.1)
                from: toInt(0.0)
                to: toInt(1.0)

                validator: DoubleValidator {
                    bottom: secondaryLabelOpacitySpinBox.from
                    top: secondaryLabelOpacitySpinBox.to
                    decimals: 1
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: (value, locale) => {
                    return Number(fromInt(value)).toLocaleString(locale, 'f', 1);
                }

                valueFromText: (text, locale) => {
                    return Math.round(toInt(Number.fromLocaleString(locale, text)));
                }

                onValueChanged: { cfg_secondaryLabelOpacity = fromInt(value); }

                function toInt(value) {
                    return value * 10;
                }

                function fromInt(value) {
                    return value / 10;
                }
            }
        }

    }
}
