/*
    SPDX-FileCopyrightText: 2023 Oliver Beard <olib141@outlook.com>
    SPDX-License-Identifier: MIT
*/

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2

import org.kde.kcmutils as KCM
import org.kde.kirigami as Kirigami

KCM.SimpleKCM {

    property double cfg_updateInterval

    // HACK: Present to suppress errors
    property string cfg_sensors
    property string cfg_sensorsDefault
    property bool cfg_showUnit
    property bool cfg_showUnitDefault
    property bool cfg_swapLabels
    property bool cfg_swapLabelsDefault
    property bool cfg_isoFont
    property bool cfg_isoFontDefault
    property double cfg_primaryFontScale
    property double cfg_primaryFontScaleDefault
    property double cfg_secondaryFontScale
    property double cfg_secondaryFontScaleDefault
    property double cfg_secondaryLabelOpacity
    property double cfg_secondaryLabelOpacityDefault
    property bool cfg_secondaryLabelCentered
    property bool cfg_secondaryLabelCenteredDefault
    property double cfg_updateIntervalDefault

    onCfg_updateIntervalChanged: {
        updateIntervalSpinBox.value = updateIntervalSpinBox.toInt(cfg_updateInterval);
    }

    Component.onCompleted: cfg_updateIntervalChanged()

    Kirigami.FormLayout {

        QQC2.SpinBox {
            id: updateIntervalSpinBox

            Kirigami.FormData.label: "Update interval:"

            stepSize: toInt(0.1)
            from: toInt(1)
            to: toInt(5)

            validator: DoubleValidator {
                bottom: updateIntervalSpinBox.from
                top: updateIntervalSpinBox.to
                decimals: 1
                notation: DoubleValidator.StandardNotation
            }

            textFromValue: (value, locale) => {
                return Number(fromInt(value)).toLocaleString(locale, 'f', 1) + " s";
            }

            valueFromText: (text, locale) => {
                return Math.round(toInt(Number.fromLocaleString(locale, text.split(" ")[0])));
            }

            onValueChanged: cfg_updateInterval = fromInt(value)

            function toInt(value) {
                return value * 10;
            }

            function fromInt(value) {
                return value / 10;
            }
        }

    }
}
