/*
    SPDX-FileCopyrightText: 2023 Oliver Beard <olib141@outlook.com>
    SPDX-License-Identifier: MIT
*/

import QtQuick
import org.kde.plasma.configuration

ConfigModel {

    ConfigCategory {
         name: i18n("Sensors")
	 icon: Qt.resolvedUrl('../images/fan-Icon-made-by-Freepik-from-www.flaticon.com.svg')
         source: "config/ConfigSensors.qml"
    }

    ConfigCategory {
         name: i18n("Appearance")
         icon: "preferences-desktop-color"
         source: "config/ConfigAppearance.qml"
    }

    ConfigCategory {
         name: i18n("Misc")
         icon: "preferences-system-other"
         source: "config/ConfigMisc.qml"
    }

}
